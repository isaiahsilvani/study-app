package com.example.studyapp.model.entities

import android.util.Log
import com.example.studyapp.view.FlashCardState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class FlashCard {
    // This is where we do our methods for FlashCard functionality and we put it inside a Default thread for CPU intensive work
    val TAG = "FLASHCARD ENTITY"

    private var _flashCardState: MutableStateFlow<FlashCardState> = MutableStateFlow(FlashCardState())
    val flashCardState: StateFlow<FlashCardState> get() = _flashCardState

    val scope = CoroutineScope(Dispatchers.Default + SupervisorJob())

    fun nextClick(listOfTopics: List<String>) {
        val currentIndex = listOfTopics.indexOf(_flashCardState.value.topic)
        val lastIndex = listOfTopics.size - 1
        scope.launch {
            with (_flashCardState) {
                // If the state is null, set the state to the first element
                if (value.topic == null || currentIndex == lastIndex) {
                    value = value.copy(topic = listOfTopics[0])
                } else {
                    value = value.copy(topic = listOfTopics[currentIndex + 1])
                }
            }
            Log.e(TAG, "Executing in thread ${Thread.currentThread().name}")
            Log.e(TAG, "The topic now is ${_flashCardState.value.topic}")
        }
    }

    fun previousClick(listOfTopics: List<String>) {
        val currentIndex = listOfTopics.indexOf(_flashCardState.value.topic)
        val lastIndex = listOfTopics.size - 1
        scope.launch {
            with (_flashCardState) {
                if (value.topic == null || currentIndex == 0) {
                    value = value.copy(topic = listOfTopics[lastIndex])
                    Log.e(TAG, _flashCardState.value.topic.toString())
                } else {
                    value = value.copy(topic = listOfTopics[currentIndex - 1])
                }
                Log.e(TAG, "Executing in Default thread ${Thread.currentThread().name}")
                Log.e(TAG, "The topic now is ${_flashCardState.value.topic}")
            }
        }

    }

}