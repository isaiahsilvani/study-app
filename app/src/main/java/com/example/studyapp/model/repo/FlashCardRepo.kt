package com.example.studyapp.model.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.studyapp.model.entities.FlashCard
import com.example.studyapp.view.FlashCardState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

object FlashCardRepo {
    val TAG = "FlashCardRepo"
    private val listOfTopics = listOf("Math", "Science", "Art", "History", "Dave Chappele", "Mad Men", "Breaking Bad",
        "Family Guy", "Vice News", "Gym", "English", "Computer Science", "Your Mom")

    val scope = CoroutineScope(Dispatchers.Default + SupervisorJob())
    private val flashCard by lazy {
        FlashCard()
    }

    val currentTopic: StateFlow<FlashCardState> = flashCard.flashCardState

    fun handleClick(option: String) {
        scope.launch {
            when (option) {
                "next" -> flashCard.nextClick(listOfTopics)
                "previous" -> flashCard.previousClick(listOfTopics)
                else -> "Your mom...."
            }
        }
    }
}