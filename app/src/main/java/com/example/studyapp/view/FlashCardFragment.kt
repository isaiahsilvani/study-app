package com.example.studyapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import com.example.studyapp.databinding.FragmentFlashCardBinding
import com.example.studyapp.viewmodel.FlashCardViewModel

class FlashCardFragment : Fragment() {
    val TAG = "FlashCardFragment"

    /**
     * To access the widgets use the code below
     *
     * Topic TextView -> binding.tvTopic
     * Close Button -> binding.ivClose
     * Previous Button -> binding.btnPrev
     * Next Button -> binding.btnNext
     *
     * To enable and disable a view use example below
     * binding.btnNext.isEnabled
     */
    private var _binding: FragmentFlashCardBinding? = null
    private val binding get() = _binding!!
    private val FlashCardVM by viewModels<FlashCardViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFlashCardBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root.also{ initViews() }.also { initObservers() }

    private fun initViews() {
        with(binding) {
            val buttons = listOf(btnNext, btnClose, btnPrev)
            for (button in buttons) {
                button.setOnClickListener {
                    if (button.id == btnClose.id) {
                        val action = FlashCardFragmentDirections.actionGridFragmentToStartFragment()
                        findNavController().navigate(action)
                    } else {
                        FlashCardVM.handleClick(button.tag.toString())
                    }
                }
            }
        }
    }
    private fun initObservers() {
        FlashCardVM.flashCardState.observe(viewLifecycleOwner) {
            val topic = it.topic
            Log.e(TAG, "INIT OBSERVERS HIT MANE")
            if (topic != null) {
                binding.tvTopic.text = topic
            } else {
                Log.e(TAG, "NO TOPIC DISPLAYED")
                binding.tvTopic.text = "No Topic Displayed"
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}