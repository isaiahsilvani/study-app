package com.example.studyapp.view

data class FlashCardState(
    var topic: String? = null
)