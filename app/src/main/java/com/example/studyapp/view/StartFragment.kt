package com.example.studyapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.studyapp.R
import com.example.studyapp.databinding.FragmentStartBinding
import com.example.studyapp.viewmodel.FlashCardViewModel

class StartFragment: Fragment(R.layout.fragment_start) {
    lateinit var binding: FragmentStartBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartBinding.inflate(layoutInflater)
        initViews()
        return binding.root
    }

    private fun initViews() {
        with(binding) {
            button.setOnClickListener {
                val action = StartFragmentDirections.actionStartFragmentToGridFragment()
                findNavController().navigate(action)
            }
        }
    }
}