package com.example.studyapp.viewmodel

interface ButtonInterface {
    fun nextClick()
    fun prevClick()
}