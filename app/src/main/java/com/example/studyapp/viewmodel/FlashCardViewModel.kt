package com.example.studyapp.viewmodel

import android.util.Log
import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.studyapp.model.entities.FlashCard
import com.example.studyapp.model.repo.FlashCardRepo
import com.example.studyapp.view.FlashCardState
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.handleCoroutineException
import kotlinx.coroutines.launch
import java.lang.Error

class FlashCardViewModel: ViewModel() {
    val TAG = "FlashCardViewMODEL"
    // Setting up private state variable thats connected to our stata data class
    private val _flashCardState: MutableLiveData<FlashCardState> = MutableLiveData(FlashCardState())
    // Setting up public state variable that gets state from private state varialb so that
    // We can observe state changes in the View and update UI accordingly
    val flashCardState: LiveData<FlashCardState> get() = _flashCardState
    //
    private val repo: FlashCardRepo = FlashCardRepo

    init {
        viewModelScope.launch {
            repo.currentTopic.collect  {
                _flashCardState.value = it
            }
        }
    }

    fun handleClick(tag: String) {
        viewModelScope.launch {
            Log.e(TAG, "Executing in viewModelScopethread ${Thread.currentThread().name}")
            repo.handleClick(tag)
        }
    }
}